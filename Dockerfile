#RUN apt-get update -y && \
#apt-get install -y python-pip python-dev

#dockerfile
#python:3.10.4
#FROM python:latest
#ADD source file path | dest path (. base container path)
#ADD app.py .

FROM python:latest


# Create directory in container image for app code
RUN mkdir -p /usr/src/app

# Copy app code (.) to /usr/src/app in container image
COPY . /usr/src/app

# Set working directory context
WORKDIR /usr/src/app


#RUN using to install requi packages
#RUN pip install Flask

RUN pip install -r requirements.txt

#cmd exc when container start
#CMD ["python", "usr/src/app/app.py"]
#as exec
ENTRYPOINT [ "python"] 
CMD [ "py.py" ]